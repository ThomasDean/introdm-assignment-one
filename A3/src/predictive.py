import json
import random


def predict(point: list, predictor: dict, fav, unfav):
    result: str
    for feature in range(0, len(point)):
        checker = str(feature) + ", '" + point[feature] + "'"
        if checker in predictor:
            if type(predictor[checker]) == dict:
                result = predict(point, predictor[checker], fav, unfav)
            else:
                if not predictor[checker][0].isdigit():
                    result = predictor[checker]
                else:
                    perc = float(predictor[checker][0:5])
                    if random.random() <= perc:
                        result = fav
                    else:
                        result = unfav
    return result


def tester(test: list, predictor: dict, value: int, fav, unfav):
    total = 0
    right = 0
    for line in test:
        check = predict(line, predictor, fav, unfav)
        if check == line[value]:
            right += 1
        total += 1
    result = right / total
    return result
