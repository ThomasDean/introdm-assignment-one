# =======================================#
# Imports                               #
# =======================================#
# Python Imports
import csv
import random

# External Imports

# Mushroom Imports

# =======================================#
# Globals & Constants                   #
# =======================================#

__all__ = (
    'parse_init_txt',
    'parse_run_csv',
    'write_testing_csv',
    'write_training_csv',
)


# =======================================#
# Public Methods                        #
# =======================================#

def parse_init_txt(path: str):
    voting_data = []
    with open(path, 'r') as file:
        for line in file:
            line = line.replace('\n', '')  # The end of the line is \n and will be reflected on the split if not removed
            row = line.split(',')  # Split on comma to get
            voting_data.append(row)  # Add new entry to data

    return voting_data


def parse_run_csv(path: str):
    with open(path, 'r') as file:
        voting_data = list(csv.reader(file))
    return voting_data


def write_testing_csv(path: str, data: list):
    with open(path, 'w') as output_file:
        output_writer = csv.writer(output_file)
        for row in data:
            output_writer.writerow(row)


def write_training_csv(path: str, data: list):
    with open(path, 'w') as output_file:
        output_writer = csv.writer(output_file)
        for row in data:
            train_or_test = random.randint(0, 4)
            if train_or_test == 2:
                pass
            else:
                output_writer.writerow(row)