from data.preprocessor import *
import os
from src.predictive import *


def ind_accuracy(cur_set: list, value: int, ruled):
    ruled_count = 0
    size = 0
    for point in cur_set:
        if point[value] == ruled:
            ruled_count += 1
        size += 1
    per_correct = ruled_count / size
    per_correct = per_correct*100
    return per_correct


def ind_majority(cur_set: list, value: int, fav, unfav):
    fav_count = 0
    unfav_count = 0
    size = 0
    for point in cur_set:
        if point[value] == fav:
            fav_count += 1
        elif point[value] == unfav:
            unfav_count += 1
        size += 1
    per_fav = fav_count / size
    per_unfav = unfav_count / size
    if per_fav >= per_unfav:
        return fav
    else:
        return unfav


def col_accuracy(cur_set: list, value: int, attribute: int, fav, unfav, rule_path):
    att_rule = {}
    split_values = []
    max_size = 0
    for point in cur_set:
        if point[attribute] not in split_values:
            split_values.append(point[attribute])
        max_size += 1
    sub_set = []
    sub_sizes = []
    for feature in split_values:
        temp = []
        size = 0
        for point in cur_set:
            if point[attribute] == feature:
                temp.append(point)
                size += 1
        sub_set.append(temp[:])
        sub_sizes.append(size)
    sub_accuracy = []
    rule = "rule for attribute "+str(attribute)+": "
    for sub in range(0, len(sub_set)):
        sub_maj = ind_majority(sub_set[sub], value, fav, unfav)
        rule = rule+"attribute value "+str(split_values[sub])+" predicts "+str(sub_maj)+", "
        sub_accuracy.append(ind_accuracy(sub_set[sub], value, sub_maj))
        rule = rule+"accuracy: "+str("{:05.2f}".format(sub_accuracy[-1]))+"%; "
        att_rule[str(attribute)+", '"+split_values[sub]+"'"] = sub_maj
    accuracy = 0
    for i in range(0, len(sub_set)):
        accuracy += ((sub_sizes[i] / max_size) * sub_accuracy[i])
    rule = rule+"total accuracy: "+str("{:05.2f}".format(accuracy))+"%\n"
    rule_path.write(rule)
    return accuracy, att_rule


def find_rule(cur_set, value: int, fav, unfav, rule_path):
    sub_sets = {}
    rules = []
    for i in range(0, len(cur_set[0])):
        temp_rule = {}
        if i != value:
            sub_sets[i],temp_rule = col_accuracy(cur_set, value, i, fav, unfav, rule_path)
            rules.append(temp_rule)
    checked = {}
    biggest_acc: int
    for feature in sub_sets:
        if len(checked) == 0:
            biggest_acc = feature
        elif sub_sets[feature] > sub_sets[biggest_acc]:
            biggest_acc = feature
        checked[feature] = sub_sets[feature]
    if biggest_acc>value:
        big_rule = biggest_acc-1
    else:
        big_rule = biggest_acc
    return biggest_acc, rules[big_rule]


def OneR(cur_set: list, value: int, fav, unfav, rule_path):
    r = open(rule_path, "w")
    rule, checker = find_rule(cur_set, value, fav, unfav, r)
    r.write("\nAttribute "+str(rule)+" has the highest accuracy")
    r.close()
    return checker


MUSHROOM_TXT_PATH = os.path.join(os.getcwd(), "..", 'data',
                                 'mushroom_data_raw.txt')  # join() and getcwd() are used in order to support all OS
MUSHROOM_TEST_CSV_PATH = os.path.join(os.getcwd(), "..", 'Created_files', 'mushroom_data.csv')
MUSHROOM_TRAIN_CSV_PATH = os.path.join(os.getcwd(), "..", 'Created_files', 'mushroom_train.csv')
MUSHROOM_RESULTS_PATH = os.path.join(os.getcwd(), 'mushroom_rules.txt')

fav = 'e'
unfav = 'p'

txt_data = parse_init_txt(MUSHROOM_TXT_PATH)
csv_data = parse_run_csv(MUSHROOM_TRAIN_CSV_PATH)
csv_test = parse_run_csv(MUSHROOM_TEST_CSV_PATH)
rule = OneR(csv_data,0,fav,unfav, MUSHROOM_RESULTS_PATH)
print(tester(csv_data, rule, 0, fav, unfav))
print(tester(csv_test, rule, 0, fav, unfav))
