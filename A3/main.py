# =======================================#
# Imports                               #
# =======================================#
# Python Imports
import csv
import os
import json

# External Imports

# Module Imports
from Data.preprocessor import *
from src.predictive import *
from src.recurse import *

# =======================================#
# Globals & Constants                   #
# =======================================#

MUSHROOM_TXT_PATH = os.path.join(os.getcwd(), 'Data',
                                 'mushroom_data_raw.txt')  # join() and getcwd() are used in order to support all OS
MUSHROOM_TEST_CSV_PATH = os.path.join(os.getcwd(), 'Created_files', 'mushroom_data.csv')
MUSHROOM_TRAIN_CSV_PATH = os.path.join(os.getcwd(), 'Created_files', 'mushroom_train.csv')
MUSHROOM_RESULTS_PATH = os.path.join(os.getcwd(), 'classifier', 'mushroom_classifier.json')
# =======================================#
# Public Methods                        #
# =======================================#

# =======================================#
# Code                                  #
# =======================================#

fav = 'e'
unfav = 'p'

txt_data = parse_init_txt(MUSHROOM_TXT_PATH)

# write_training_csv(MUSHROOM_TRAIN_CSV_PATH, txt_data)
# write_testing_csv(MUSHROOM_TEST_CSV_PATH, txt_data)

csv_data = parse_run_csv(MUSHROOM_TRAIN_CSV_PATH)
csv_test = parse_run_csv(MUSHROOM_TEST_CSV_PATH)

results = build_tree(csv_data, 0, fav, unfav, [])
with open(MUSHROOM_RESULTS_PATH, 'w') as out_file:
    json.dump(results, out_file)

mush_file = open(MUSHROOM_RESULTS_PATH)
mush_str = mush_file.read()
mush_data = json.loads(mush_str)
print(tester(csv_data, mush_data, 0, fav, unfav))
print(tester(csv_test, mush_data, 0, fav, unfav))
