from collections import defaultdict
from itertools import chain, combinations


def find_subsets(itemset: iter):
    """
    Finds all subsets of the itemset.
    :param itemset: An iterable of sets.
    :return: A list of frozensets.
    """
    subset_list = chain(*[combinations(itemset, i + 1) for i, a in enumerate(itemset)])
    return map(frozenset, [x for x in subset_list])


def frequent_items(itemset: iter, database: list, min_support: float, support_dictionary: defaultdict):
    """
    Finds all items in an itemset with a support greater than min_support in the database.
    :param itemset: An iterable of items.
    :param database: A list of itemsets.
    :param min_support: A float value between 0.0 and 1.0
    :param support_dictionary: A dict of support values to itemsets
    :return: A subset of all items with greater than or equal support to the min_support.
    """
    new_itemset = set()
    this_set = defaultdict(int)
    for item in itemset:
        for transaction in database:
            if item.issubset(transaction):
                support_dictionary[item] += 1
                this_set[item] += 1
    for item, count in this_set.items():
        support = float(count) / len(database)
        if support >= min_support:
            new_itemset.add(item)
    return new_itemset


def join_set(itemset: set, length: int):
    """
    Joins a set with itself and returns the itemsets that are less than or equal to length.
    :param itemset: A set of items.
    :param length: The maximum length of itemsets.
    :return: A set of itemsets that are less than or equal to length.
    """
    return set([i.union(j) for i in itemset for j in itemset if len(i.union(j)) == length])


def apriori_main(database: list, min_support: float, min_confidence: float):
    """
    Run the Apriori Association Mining Algorithm on the given database.
    :param database: A list of frozensets.
    :param min_support: A float value between 0.0 and 1.0
    :param min_confidence: A float value between 0.0 and 1.0
    """
    if type(database) is not list:
        raise ValueError("The database must be a list of frozensets")
    if not 0 <= min_support <= 1:
        raise ValueError("The min_support parameter must be a float value between 0.0 and 1.0")
    if not 0 <= min_confidence <= 1:
        raise ValueError("The min_confidence parameter must be a float value between 0.0 and 1.0")
    support_dictionary = defaultdict(int)    # key: n-itemsets, value: support of that set
    set_dictionary = dict()                  # key: n-itemsets, value: list of itemsets
    itemsets = set()
    counter = 0
    for transaction in database:
        if type(transaction) is list:
            transaction = frozenset(transaction)
            database[counter] = transaction.copy()
        for item in transaction:
            itemsets.add(frozenset([item]))

    candidates = frequent_items(itemsets, database, min_support, support_dictionary)

    n = 2
    while candidates != set([]):
        print(n)
        set_dictionary[n-1] = candidates
        candidates = join_set(candidates, n)
        new_candidates = frequent_items(candidates, database, min_support, support_dictionary)
        candidates = new_candidates
        n = n + 1

    def get_support(_item):
            """local function which Returns the support of an item"""
            return float(support_dictionary[_item])/len(database)

    items = []
    for key, value in set_dictionary.items():
        for item in value:
            items.append(str(item) + ": " + str(get_support(item)))

    rules = []
    for index, itemsets in set_dictionary.items():
        # for every set greater than 1
        if index > 1:
            for itemset in itemsets:
                # generate possible subsets
                subsets = find_subsets(itemset)
                for precedent in subsets:
                    antecedent = itemset - precedent
                    if len(antecedent) > 0:
                        # confidence{A->B} = support{A,B} / support{A}
                        confidence = get_support(itemset) / get_support(precedent)
                        if confidence >= min_confidence:
                            # lift{A->B} = support{A,B} / support{A} * support{B}
                            lift = get_support(itemset) / (get_support(precedent) * get_support(antecedent))
                            rules.append("{" + str(precedent)[10:-1] + " => " + str(antecedent)[10:-1] +
                                         "}: confidence = " + str(confidence)[:8] + ", lift = " + str(lift)[:8])
    return items, rules
