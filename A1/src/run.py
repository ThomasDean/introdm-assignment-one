import os
import pickle
import sys
import urllib.request
from pathlib import Path

from hero_list import make_hero_list
from apriori import apriori_main
from csv_to_dict import csv_to_dict


def run():
    __loc__ = str(sys.path[0][:-3])
    __data__ = __loc__ + "/data"
    raw_data = __data__ + "/matches_small.csv"
    cleaned_data = __data__ + "/cleaned_data"
    database = __data__ + "/database"

    if not Path(__data__).exists():
        print("Making data directory...")
        os.makedirs(__data__)

    if Path(database).exists():
        if Path(cleaned_data).exists():
            print("Deleting cleaned_data pickle file...")
            Path(cleaned_data).unlink()
        print("Opening the database pickle file...")
        infile = open(database, 'rb')
        print("Loading the data from the database pickle file...")
        dataset = pickle.load(infile)
        itemsets, rules = apriori_main(dataset[:10000], 0.005, 0)
        output = open(__loc__ + '/out/Output.txt', 'w')
        output.write("Itemsets w/ Support: \n\n")
        for itemset in itemsets:
            output.write(itemset + "\n")
        output.write("Rules: \n\n")
        for rule in rules:
            output.write(str(rule) + "\n")
        output.close()

    elif Path(cleaned_data).exists():
        make_hero_list()
        run()

    elif Path(raw_data).exists():
        csv_to_dict(raw_data)
        run()

    else:
        print("Downloading matches_small.csv from the dota-match-dumps api...")
        urllib.request.urlretrieve("https://storage.googleapis.com/dota-match-dumps/matches_small.csv",
                                   raw_data)
        run()


if __name__ == '__main__':
    run()
